from quant.utils import EventEngine, abstract_method


class Api:
    all_api = []

    def __init__(self, keys, account=None):
        type(self).all_api.append(self)

        if account is None:
            from quant.accounts.accounts import Account
            account = Account(None, keys)
            account.api = self

        self.keys = keys
        self.account = account

        self.api_key = keys['api_key']
        self.secret_key = keys['secret_key']
        self.init()

    def init(self):
        pass

    @abstract_method
    def add_symbol(self, symbol):
        pass

    @abstract_method
    def query_balance(self, symbol=None):
        pass

    @abstract_method
    def query_all_balance(self):
        pass

    @abstract_method
    def query_margin(self, symbol=None):
        pass

    @abstract_method
    def query_all_margin(self):
        pass

    @abstract_method
    def query_position(self, symbol=None):
        pass

    @abstract_method
    def query_all_position(self):
        pass

    @abstract_method
    def query_open_orders(self, symbol=None):
        pass

    @abstract_method
    def query_all_open_orders(self):
        pass

    @abstract_method
    def place_order(self, order):
        client_id = ...
        return client_id

    @abstract_method
    def cancel_order(self, order):
        pass

    @abstract_method
    def modify_order(self, old, new, **options):
        pass

    @abstract_method
    def cancel_all(self):
        pass

    @abstract_method
    def join(self):
        pass

    @abstract_method
    def ignore_offset(self, ignore):
        pass

    @abstract_method
    def set_ws_mode(self, ws=True):
        pass


class Spi:
    all_spi = []

    def __init__(self, keys, account=None):
        type(self).all_spi.append(self)

        if account is None:
            from quant.accounts.accounts import Account
            account = Account(None, keys)
            account.spi = self

        self.keys = keys
        self.account = account

        self.api_key = keys['api_key']
        self.secret_key = keys['secret_key']
        self.init()

    def init(self):
        pass

    @abstract_method
    def add_symbol(self, symbol):  # auto connect socket.
        pass
