from quant.markets import Channel, Handler
from quant.const import Exchange
from quant.exchanges._factory import add_factory
from quant.exchanges.basics import ApiRouter, SpiRouter, FunctionsRouter
from quant.exchanges.util import get_agent_name
from quant.exchanges.huobi_spot import HuobiSpotChannel, HuobiSpotHandler, HuobiSpotApi, HuobiSpotSpi, HuobiSpotFunctions
from quant.exchanges.huobi_swap_usdt import HuobiSwapUsdtChannel, HuobiSwapUsdtHandler, HuobiSwapUsdtApi, HuobiSwapUsdtSpi, HuobiSwapUsdtFunctions


class HuobiChannel(Channel):
    agent: Channel

    def init(self):
        agent_name = get_agent_name(self.symbol)
        agent_cls = _channel_agent_map[agent_name]
        self.agent = agent_cls(self.event, self.exchange, self.symbol, self.frequency, self.markets)

    def connect(self):
        return self.agent.connect()

    def disconnect(self):
        return self.agent.disconnect()

    def is_open(self):
        return self.agent.is_open()


class HuobiHandler(Handler):
    agent: Handler

    def init(self):
        agent_name = get_agent_name(self.symbol)
        agent_cls = _handler_agent_map[agent_name]
        self.agent = agent_cls(self.event, self.exchange, self.symbol, self.frequency, self.markets)

    def process(self, routing_key, recv_time, raw):
        return self.agent.process(routing_key, recv_time, raw)


class HuobiApi(ApiRouter):
    def _create_agent(self):
        params = (self.keys, self.account)
        self.agent_map = {n: cls(*params) for n, cls in _api_agent_map.items()}

    def _get_agent(self, symbol):
        agent_name = get_agent_name(symbol)
        try:
            agent = self.agent_map[agent_name]
        except KeyError:
            err = 'Agent for {} not implemented!'.format(symbol)
            raise NotImplementedError(err)
        return agent


class HuobiSpi(SpiRouter):
    def _create_agent(self):
        params = (self.keys, self.account)
        self.agent_map = {n: cls(*params) for n, cls in _spi_agent_map.items()}

    def _get_agent(self, symbol):
        agent_name = get_agent_name(symbol)
        try:
            agent = self.agent_map[agent_name]
        except KeyError:
            err = 'Agent for {} not implemented!'.format(symbol)
            raise NotImplementedError(err)
        return agent


class HuobiFunctions(FunctionsRouter):
    def _create_agent(self):
        self.agent_map = {n: cls() for n, cls in _exchange_function_map.items()}

    def _get_agent(self, symbol):
        agent_name = get_agent_name(symbol)
        return self.agent_map[agent_name]


_channel_agent_map =     {'spot': HuobiSpotChannel,   'usdt.swap': HuobiSwapUsdtChannel}
_handler_agent_map =     {'spot': HuobiSpotHandler,   'usdt.swap': HuobiSwapUsdtHandler}
_api_agent_map =         {'spot': HuobiSpotApi,       'usdt.swap': HuobiSwapUsdtApi}
_spi_agent_map =         {'spot': HuobiSpotSpi,       'usdt.swap': HuobiSwapUsdtSpi}
_exchange_function_map = {'spot': HuobiSpotFunctions, 'usdt.swap': HuobiSwapUsdtFunctions}

add_factory(Exchange.Huobi, HuobiChannel, HuobiHandler, HuobiApi, HuobiSpi, HuobiFunctions)
