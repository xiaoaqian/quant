






















import requests
from quant.utils import logging


def get_ws_endpoint(host):
    path = '/api/v1/bullet-public'
    url = host + path
    res = requests.post(url, timeout=3)
    try:
        data = res.json()['data']
        token = data['token']
        servers = data['instanceServers']
        servers = [s['endpoint'] for s in servers]
        return token, servers
    except:
        logging.warn('Kucoin get ws endpoint fail!')
        return None, None


def is_welcome(data):
    tp = data['type']
    if tp == 'welcome' or tp == 'ack':
        return True
    return False


if __name__ == '__main__':
    a = get_ws_endpoint('https://api.kucoin.com')
    print(a)
