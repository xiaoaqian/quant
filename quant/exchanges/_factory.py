from quant.markets import Channel, Handler, Functions
from quant.accounts.api import Api, Spi


class ExchangeFactory:
    channel_cls = Channel
    handler_cls = Handler
    api_cls = Api
    spi_cls = Spi
    function_cls = Functions

    def __init__(self, channel_cls, handler_cls, api_cls, spi_cls, function_cls):
        self.__setattr__('channel_cls', channel_cls)
        self.__setattr__('handler_cls', handler_cls)
        self.__setattr__('api_cls', api_cls)
        self.__setattr__('spi_cls', spi_cls)
        self.__setattr__('function_cls', function_cls)


class DefaultCls:
    def __init__(self, *args, **kwargs):
        pass

    def __getattr__(self, item):
        return self._default_method

    def _default_method(self, *args, **kwargs):
        pass


def add_factory(exchange, channel_cls, handler_cls, api_cls, spi_cls, function_cls):
    factory = ExchangeFactory(channel_cls, handler_cls, api_cls, spi_cls, function_cls)
    _factory_map[exchange] = factory


def get_factory(exchange):
    result: ExchangeFactory = _factory_map[exchange]
    return result


_factory_map = {}
add_factory(None, *[DefaultCls]*5)


if __name__ == '__main__':
    a = DefaultCls()

    a.do()
