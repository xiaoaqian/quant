from quant.markets import Channel, Handler
from quant.accounts.api import Api, Spi
from quant.const import Exchange
from quant.exchanges._factory import add_factory
from quant.exchanges.basics import ApiRouter, SpiRouter, FunctionsRouter
from quant.exchanges.util import get_agent_name
from quant.exchanges.gate_swap_usdt import GateSwapFunctions
from quant.exchanges.gate_spot import GateSpotChannel, GateSpotHandler, GateSpotApi, GateSpotSpi, GateSpotFunctions


class GateChannel(Channel):
    agent: Channel

    def init(self):
        agent_name = get_agent_name(self.symbol)
        agent_cls = _channel_agent_map[agent_name]
        self.agent = agent_cls(self.event, self.exchange, self.symbol, self.frequency, self.markets)

    def connect(self):
        return self.agent.connect()

    def disconnect(self):
        return self.agent.disconnect()

    def is_open(self):
        return self.agent.is_open()


class GateHandler(Handler):
    agent: Handler

    def init(self):
        agent_name = get_agent_name(self.symbol)
        agent_cls = _handler_agent_map[agent_name]
        self.agent = agent_cls(self.event, self.exchange, self.symbol, self.frequency, self.markets)

    def process(self, routing_key, recv_time, raw):
        return self.agent.process(routing_key, recv_time, raw)


class GateApi(ApiRouter):
    def _create_agent(self):
        params = (self.keys, self.account)
        self.agent_map = {n: cls(*params) for n, cls in _api_agent_map.items()}

    def _get_agent(self, symbol):
        agent_name = get_agent_name(symbol)
        try:
            agent: Api = self.agent_map[agent_name]
        except KeyError:
            err = 'Agent for {} not implemented!'.format(symbol)
            raise NotImplementedError(err)
        return agent


class GateSpi(SpiRouter):
    def _create_agent(self):
        params = (self.keys, self.account)
        self.agent_map = {n: cls(*params) for n, cls in _spi_agent_map.items()}

    def _get_agent(self, symbol):
        agent_name = get_agent_name(symbol)
        try:
            agent: Spi = self.agent_map[agent_name]
        except KeyError:
            err = 'Agent for {} not implemented!'.format(symbol)
            raise NotImplementedError(err)
        return agent


class GateFunctions(FunctionsRouter):
    def _create_agent(self):
        self.agent_map = {n: cls() for n, cls in _exchange_function_map.items()}

    def _get_agent(self, symbol):
        agent_name = get_agent_name(symbol)
        return self.agent_map[agent_name]


from quant.exchanges.util import NullCls

GateSwapUsdtChannel = NullCls
GateSwapUsdtHandler = NullCls
GateSwapUsdtApi = NullCls
GateSwapUsdtSpi = NullCls


_channel_agent_map = {'spot': GateSpotChannel, 'usdt.swap': GateSwapUsdtChannel}
_handler_agent_map = {'spot': GateSpotHandler, 'usdt.swap': GateSwapUsdtHandler}
_api_agent_map = {'spot': GateSpotApi, 'usdt.swap': GateSwapUsdtApi}
_spi_agent_map = {'spot': GateSpotSpi, 'usdt.swap': GateSwapUsdtSpi}
_exchange_function_map = {'spot': GateSpotFunctions, 'usdt.swap': GateSwapFunctions}

add_factory(Exchange.Gate, GateChannel, GateHandler, GateApi, GateSpi, GateFunctions)


if __name__ == '__main__':
    def try_funcs():
        from quant.exchanges import get_factory

        gate_fac = get_factory('Gate')
        funcs = gate_fac.function_cls()
        a = funcs.get_all_ticker()
        li = list(a.items())

        li = sorted(li, key=lambda x: x[-1]['vol'], reverse=True)
        for i in li:
            print(i)

    def try_funcs_2():
        functions = GateFunctions()
        a = functions.get_value_factor('btc/usdt')
        print(a)

    try_funcs_2()
