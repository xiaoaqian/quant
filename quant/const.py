class Event:
    Book = 'Book'
    Trade = 'Trade'
    Ticker = 'Ticker'


class UserEvent:
    Position = 'Position'  # {symbol: {'long': 1, 'short': 1, 'position': 0}, ...}
    Balance = 'Balance'
    Margin = 'Margin'
    Order = 'Order'
    OpenOrders = 'OpenOrders'
    Deal = 'Deal'  # (order, deal_amount)


class ApiType:
    Api = 'Api'
    Spi = 'Spi'


class Exchange:
    Okex = 'Okex'
    Binance = 'Binance'
    Huobi = 'Huobi'
    Gdax = 'Gdax'
    Gate = 'Gate'
    Kucoin = 'Kucoin'


class DataFrequency:
    Normal = 'Normal'
    Low = 'Low'
    High = 'High'


class OrderType:
    Limit = 'Limit'
    Market = 'Market'
    PostOnly = 'PostOnly'
    IOC = 'IOC'


class OrderStatus:
    """
    若有修改：1.修改ready_to_cancel. 2.consider_satisfy. 3.to_match_status(for simulate)
    """

    Placing = 'Placing'
    Pending = 'Pending'

    Canceling = 'Canceling'
    Canceled = 'Canceled'

    PartFilled = 'PartFilled'
    FullyFilled = 'FullyFilled'

    PlaceFailed = 'PlaceFailed'
    CancelFailed = 'CancelFailed'

    Modifying = 'Modifying'


class Offset:
    Open = 'Open'
    Close = 'Close'


class SimMatchingMode:
    Normal = 'Normal'
    CrossIn = 'CrossIn'
    Book = 'Book'
    CrossInBook = 'CrossInBook'


NAME_CHANNEL_CLOSE = 'CHANNEL_CLOSE'
