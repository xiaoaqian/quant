import requests
from quant.utils import catch_exception


def ticker_okex_spot():
    """
    without __/btc
    """
    url = 'https://www.okex.com/api/v5/market/tickers?instType=SPOT'

    response = requests.get(url, timeout=2)
    j = response.json()
    data = j['data']

    result = {}
    for d in data:
        symbol = d['instId'].replace('-', '/').lower()
        if not symbol.endswith('usdt'):
            continue
        result[symbol] = {
            'price': float(d['last']),
            'vol': float(d['volCcy24h']) * _usdt
        }
    return result


def ticker_bn_spot():
    """
    without __/btc
    """
    url = 'https://api.binance.com/api/v3/ticker/24hr'
    response = requests.get(url, timeout=3)
    j = response.json()

    result = {}
    for d in j:
        symbol = d['symbol'].lower()
        if not symbol[-4:] == 'usdt':
            continue
        symbol = '{}/{}'.format(symbol[:-4], symbol[-4:])

        result[symbol] = {
            'price': float(d['lastPrice']),
            'vol': float(d['quoteVolume']) * _usdt
        }
    return result


def ticker_huobi_spot():
    pass


_usdt = 6.5


if __name__ == '__main__':
    # a = ticker_okex_spot()
    a = ticker_bn_spot()
    for i, j in a.items():
        print(i, j)

    li = list(a.items())
    print(li)
    li.sort(key=lambda x: x[1]['vol'], reverse=True)
    for i in li:
        print(i)
























