

























import sys
sys.path.append('/root/code')

from quant.markets.simulate import SimMarketReal


if __name__ == '__main__':
    def try_sim_real():

        from quant.accounts import SimAccounts
        from quant.utils import set_test_mode
        from strategy.settings import read_setting
        from strategy import read_strategy
        from run import build_market_data
        set_test_mode()

        setting = read_setting('fair_maker2_bn')

        setting.strategy_id = 'sim'
        setting.run_service = True

        symbol = 'gmt/usdt'
        setting.symbol = symbol
        setting.pricing_param['symbol'] = symbol
        build_market_data(setting, symbol)

        markets = SimMarketReal()
        accounts = SimAccounts(markets)
        accounts.set_latency(20)
        accounts.set_balance({'usdt': {'free': 20000, 'frozen': 0}})

        strategy = read_strategy('FairMaker2')
        strategy = strategy(markets, accounts, setting)
        strategy.start()

        strategy.account.info_engine.show_problems()

        while True:
            input(':')

    try_sim_real()




















