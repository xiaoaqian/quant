from quant import Accounts, Order


# ----------实例化账户系统--------
accounts = Accounts()


# ----------创建账户-------------
key = {
    'api_key': '...',  # 填写api
    'secret_key': '...'  # 填写api密钥
}
acc = accounts.create_account('Binance', key)  # 创建一个binance账户


# ----------设置交易品种----------
acc.add_symbol('btc/usdt')              # 添加btc/usdt现货
acc.add_symbol('eth/usdt.swap')         # eth/usdt永续合约
# 设置后acc自动通过ws接口维护用户该品种的order、balance、margin、position信息


# ----------查询现货账户余额-------
acc.api.query_all_balance()             # 查询所有余额
acc.api.query_balance('eos/usdt')       # 查询eos/usdt交易对的余额，即eos余额和usdt余额
acc.api.query_balance()                 # 缺省symbol，查询添加过的交易对余额，即查询btc余额和usdt余额
acc.api.join()                          # 阻塞至所有api操作完成，即收到http response并更新账户信息完成
print(acc.balance)                      # 打印账户余额


# ----------查询合约保证金余额-----
acc.api.query_all_margin()              # 查询所有保证金
acc.api.query_margin('eos/usdt.swap')   # 查询eos/usdt永续合约的保证金
acc.api.query_margin()                  # 缺省symbol，查询添加过的交易对保证金，即查询eth/usdt永续合约的保证金
acc.api.join()                          # 阻塞至查询完成
print(acc.margin)                       # 打印保证金


# ----------查询当前挂单----------
acc.api.query_all_open_orders()         # 查询所有挂单
acc.api.query_open_orders('eos/usdt')   # 查询eos/usdt交易对挂单
acc.api.query_open_orders()             # 缺省symbol，查询添加过的交易对挂单，即查询btc/usdt现货、eth/usdt永续的挂单
acc.api.join()                          # 阻塞至查询完成
print(acc.orders)                       # 打印当前挂单


# ----------查询合约持仓---------
acc.api.query_all_position()            # 查询所有合约持仓
acc.api.query_position('eos/usdt.swap') # 查询eos/usdt永续合约持仓
acc.api.query_position()                # 缺省symbol，查询添加过的交易对挂单，eth/usdt永续合约持仓
acc.api.join()                          # 阻塞至查询完成
print(acc.position)                     # 打印合约持仓


# ----------下单---------------
order = Order('btc/usdt', 'buy', 20000, 0.01)
acc.api.place_order(order)


# ----------账户信息的自动维护---
import time

print(acc.orders)                       # 当前为空
print(acc.margin)                       # 当前为空
print(acc.position)                     # 当前为空

acc.api.add_symbol('doge/usdt.swap')    # 添加交易对开始自动维护账户
order = Order('doge/usdt.swap', 'buy', 0.09, 1000)  # 下单，以低于市场价挂单买入
acc.api.place_order(order)                          # 下单，以低于市场价挂单买入
print(acc.orders)                       # 打印订单，包含刚发出的订单，其订单状态为Placing——发送中

time.sleep(1)                           # 等待交易所回报
print(acc.orders)                       # 打印订单，包含已挂单的订单，其订单状态为Pending——已挂单


order = Order('doge/usdt.swap', 'buy', 0.12, 1000)  # 下单，以高于市场价立即买入
acc.api.place_order(order)                          # 下单，以高于市场价立即买入
print(acc.orders)                       # 打印订单，包含刚发出的订单，其订单状态为Placing——发送中

time.sleep(1)                           # 等待交易所回报
print(acc.orders)                       # 打印订单，市价单已成交，当前为空
print(acc.position)                     # 打印仓位，当前持仓1000doge
print(acc.margin)                       # 打印保证金余额，即用户当前永续合约账户余额
