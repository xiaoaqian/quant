---------quant系统的使用教程---------

阅读markets_doc和markets_try.py学习行情模块

阅读accounts_doc和accounts_try.py学习账户模块

阅读quant.const.py了解交易中会使用的字段

阅读⌈推荐的项目结构⌋



----------可供参考的示例项目----------

project_alpha，展示了用quant写出的简单策略——策略1

project_beta，展示大型项目的文件结构，移植策略1，并展示策略2

project_sun_of_beach，阳光海岸项目，展示一套真实盈利策略：
    展示策略的回测、模拟、实盘、回测行情的收集
    该策略可在binance的btc/usdt与btc/busd盘口持续盈利
    目前btc交易暂免手续费(taker/maker全免)，适合学习
    若有高等级手续费优惠，该策略可在更多交易品种持续盈利
    策略运行效果见demo.png
        图中为influx数据库+grafana终端实现的交易日志系统
        图中交易品种为2022.12.09当日的活跃品种ocean/usdt.swap
