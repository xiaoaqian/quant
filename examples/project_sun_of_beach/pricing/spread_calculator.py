class SpreadCalculator:
    def __init__(self, weighting_amount):
        self.weighting_amount = weighting_amount
        self._result = None

    def calculate(self, book):
        mid = book.get_middle()
        bid = self._cal_side(book, mid, 'bid')
        ask = self._cal_side(book, mid, 'ask')
        if bid is None or ask is None:  # book厚度不足，无法计算spread
            result = None
        else:
            result = (bid + ask) / 2
        self._result = result
        return result

    def get(self):  # 读取上次计算结果
        return self._result

    def _cal_side(self, book, mid, side):
        amt = self.weighting_amount
        dist_sum = 0
        weight_sum = 0

        for p, v in book.items(side):
            dist = abs(p - mid)
            if weight_sum + v < amt:
                dist_sum += dist * v
                weight_sum += v
            else:
                v = amt - weight_sum
                dist_sum += dist * v
                weight_sum += v
                return dist_sum / weight_sum  # 计算加权平均

        return None  # 遍历book仍未达到足够权重
