import json
from argparse import ArgumentParser
from quant import SimMarkets, SimAccounts, SimDataAgent
from strategies import get_strategy
from quant.const import SimMatchingMode


def parse_arg():
    arg = ArgumentParser()
    arg.add_argument('--strategy')
    arg.add_argument('--config')
    arg.add_argument('--key')
    return arg.parse_args()


def read_config(name):
    if name is None:
        err = 'Please assign config with --config config_name'
        raise Exception(err)
    file = './configs/{}.json'.format(name)
    result = json.load(open(file))
    return result


if __name__ == '__main__':
    arg = parse_arg()
    strategy_name = arg.strategy
    key_name = arg.key
    config_name = arg.config

    config = read_config(config_name)
    key = read_config('keys')[key_name]
    config['key'] = key
    strategy_cls = get_strategy(strategy_name)
    config['database']['log_id'] = 'back_test_xxx'  # 修改日志id，可自定义为本次回测的主题如back_test(spread=5)

    markets = SimMarkets(real_time_data=False)      # SimMarkets：模拟行情中心，拥有Markets的全部接口，用法相同。用于回测或实时模拟交易
    accounts = SimAccounts(markets)                 # SimAccoutns: 模拟账户中心，拥有Accounts的全部接口，用法相同。用于回测或实时模拟交易

    markets.matching_add_market('Trade', config['exchange'], config['symbol'])  # 添加trade行情，用于撮合。类似markets.add_market，差别在于此处添加的行情不会进入strategy
    accounts.set_balance({'usdt': {'free': 1000, 'frozen': 0}, 'btc': {'free': 0, 'frozen': 0}})  # 模拟充值，充入现货账户，任意币种皆可。此后创建的所有acc都持有该余额。
    accounts.set_margin({'usdt': 1000})                          # 模拟充值，充入合约保证金。此后创建的所有acc都持有该余额。
    accounts.set_latency(ms=10)                                  # 设置下单延迟，模拟真实交易中的网络延迟。单位毫秒
    accounts.set_matching_mode(SimMatchingMode.CrossIn)          # 设置撮合模式，详见quant.const.SimMatchingMode

    agent = SimDataAgent(markets)  # SimDataAgent：用于将行情存入数据库、从数据库读取并回放数据。此处用作回放数据
    agent.connect_database('xxx.xxx.xxx.xxx', 8086, 'some_username', 'some_password')  # 链接数据库

    strategy = strategy_cls(markets, accounts, config)
    strategy.start()

    agent.start_feed('2022-01-01T00:00:00Z', '2022-01-01T01:01:01Z')         # 开始回放数据。自动选取策略订阅的所有行情进行回放。随着回放，策略开始收到行情并交易
    # 回放数据是阻塞操作，回放结束则阻塞完成
