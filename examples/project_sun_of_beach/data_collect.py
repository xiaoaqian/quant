from quant import Markets, SimDataAgent


mar = Markets()
agent = SimDataAgent(mar)  # SimDataAgent：用于将行情存入数据库、从数据库读取并回放数据。此处用作存入数据

agent.connect_database('xxx.xxx.xxx.xxx', 8086, 'some_username', 'some_password')  # 链接数据库
agent.start_collect()                                   # 开始存入数据

mar.add_market('Book', 'Binance', 'btc/usdt')           # 订阅数据，agent将自动存入所有订阅了的数据
mar.add_market('Trade', 'Binance', 'btc/usdt')
# mar.add_market('Book', 'Okex', 'eth/usdt')            # 可以添加更多数据
# mar.add_market('Trade', 'Okex', 'eth/usdt')

while True:
    input(':')
