from .standard_maker import StandardMaker


def get_strategy(name):
    if name == 'StandardMaker':
        return StandardMaker

    err = 'strategy "{}" not implemented'.format(name)
    raise NotImplementedError(err)
