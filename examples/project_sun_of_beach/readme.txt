阳光海岸项目：本项目展示一套成熟交易策略，具有收集行情、回测、模拟、实盘四大功能，现货与合约通用。


-----------策略描述-----------
    策略机制：与project_beta中的SpreadMaker相似，差别在于：
        修改定价方式
        修改动态spread
        增设日志系统
        增设适配器(adaptor)，统一现货与合约交易。
        增设执行器(Executor)，解耦交易过程。

    pricing：定价模块，本策略增设pricing文件夹，存放各种定价器。其中：
        FairCalculator：计算盘口微观价格(fair)，指导做市报价。
        采用盘口加权平均价，衡量orderbook-imbalance，属于较为普遍的高频交易因子。
        盘口均衡时：fair=mid；买方较为激进时：fair>mid；卖方较为激进时：fair<mid。

        SpreadCalculator：计算盘口平均价差(spread)，指导做市挂单距离。
        采用盘口加权平均spread，衡量市场挂单平均宽度

        用户可以自行拓展更多复杂因子、多盘口因子、book+trade因子等

    Adaptor：包括SpotAdaptor与FutureAdaptor
        接口相同、差异行为
        现货、合约均采用position指导交易。其中现货的position为虚拟position，position=持币-底仓
        策略调用Adaptor的接口，无需在意交易的是现货还是合约

    Executor：
        策略负责指导，执行器具体执行
        用户编写其它策略亦可使用该执行器
        同一策略亦可使用不同执行器改变执行风格

    日志系统：用户需安装InfluxDB数据库，可配合grafana展示内容。grafana展示效果见demo.png


-----------回测与模拟-----------
    回测、模拟采用撮合引擎实现：
        比传统的价格撮合更为精确
        另有路由队列，可以模拟下单、撤单指令的网络延迟

    quant系统编写的strategy可以实现实盘、回测、模拟通用：
        main.py：        用于实盘，使用方法为python main.py --strategy xxxStrategy --config xxxConfig --key xxxKey
        back_test.py：   用于回测，使用方法同main.py
        sim.py：         用于模拟，使用方法同main.py。
        data_collect.py：用于收集行情

    实现机制：行情中心、账户中心帮助实现通用化。如：
        实盘交易:
            m = Markets()
            a = Accounts()
            s = strategy(m, a)
        模拟交易：
            m = SimMarkets(real_time_data=True)
            a = SimAccounts(m)
            s = strategy(m, a)
        回测交易：
            m = SimMarkets(real_time_data=False)
            a = SimAccounts(m)
            s = strategy(m, a)
        即，使用实盘中心即为实盘、使用测试中心即为测试。使用方式相同，策略无需关注实盘or回测
        其中回测前需使用SimDataAgent收集行情，见data_collect.py；回测时需使用SimDataAgent回放数据，见back_test.py
        其中模拟交易相当于实时回测，采用实时行情，无需提前收集，可方便实现实时调参系统的开发

    SimMarkets:
        使用方法与同Markets，额外接口：
        matching_add_market(...)：为回测链接trade数据用于撮合。类似于add_market()、并独立于之，该接口订阅的trade数据不会推送到strategy
        用户需确保链接上述trade行情，否则撮合引擎无法工作
        具体使用参见back_test.py、sim.py

    SimAccounts:
        使用方法与同Accounts，额外接口：
        set_balance()：         充入模拟现货余额，此后创建的account拥有该余额。格式{'usdt': {'free': 100, 'frozen': 0}, ...}
        set_margin(...)：       充入模拟保证金，此后创建的account拥有该余额。格式{'usdt': 100, ...}
        set_latency(...)：      设置模拟网络延迟，单位毫秒
        set_matching_mode(...)：设置撮合模式，详见quant.const.SimMatchingMode
        具体使用参见back_test.py、sim.py

    SimDataAgent：
        __init__(markets)：    创建时传入markets中心。若收集数据：markets用于接收行情；若回测：markets用于回放行情
        connect_database(...)：链接历史行情数据库
        start_collect(...)：   开始收集行情，将收集markets添加的所有行情。开始前需链接数据库，即connect_database()
        start_feed(...)：      开始回放行情，将回放markets添加、且数据库中存在的所有行情。开始前需链接数据库，即connect_database()
        具体使用参见back_test.py、data_collect.py

    通用策略注意事项：
        编写通用策略应避免使用time.time()、time.sleep()，应采用行情时间而非本机时间
        使用markets.now替代time.time()    获取时间
        使用markets.timer.call_later(...) 执行单次定时任务
        使用markets.timer.register(...)   执行多次定时任务
