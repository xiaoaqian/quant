from .spread_maker import SpreadMaker
from .top_maker import TopMaker


def get_strategy(name):
    if name == 'SpreadMaker':
        return SpreadMaker
    if name == 'TopMaker':
        return TopMaker
    err = 'strategy "{}" not implemented'.format(name)
    raise NotImplementedError(err)
