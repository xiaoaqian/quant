import json
from argparse import ArgumentParser
from quant import Markets, Accounts
from strategies import get_strategy


def parse_arg():
    arg = ArgumentParser()
    arg.add_argument('--strategy')
    arg.add_argument('--config')
    arg.add_argument('--key')
    return arg.parse_args()


def read_config(name):
    file = './configs/{}.json'.format(name)
    result = json.load(open(file))
    return result


if __name__ == '__main__':
    arg = parse_arg()
    strategy_name = arg.strategy
    key_name = arg.key
    config_name = arg.config

    config = read_config(config_name)
    key = read_config('keys')[key_name]
    config['key'] = key
    strategy_cls = get_strategy(strategy_name)

    markets = Markets()
    accounts = Accounts()
    strategy = strategy_cls(markets, accounts, config)
    strategy.start()

    while True:
        input('')
