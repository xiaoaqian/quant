项目beta：本项目举例说明如何采用 ⌈推荐的项目结构⌋ 编写策略


-----------项目描述-----------
project_alpha中的合约TopMaker将按照⌈推荐的项目结构⌋移植到本项目
编写新的现货交易策略SpreadMaker
参考该策略，用户可以尝试编写现货版TopMaker，或编写合约版SpreadMaker


-----------策略描述-----------
SpreadMaker：双侧等距做市策略

在盘口微观价格(fair_price)上下等距离(spread)挂单做市
其中fair_price采用mean(mid, 1s)为例简单计算，即中间价的1秒均价
其中spread由策略config中的spread参数指定

本策略不融币，需持有底仓用于ask侧挂单。用户可以手动在合约市场做空对冲，以实现0头寸

限制最大仓位(max)，持仓达到最大仓位则本侧不再下单，等待对侧成交
假设单笔下单1B，最大仓位5B，底仓5B，设当前余额为bal，则：
    bal=5B：       初始状态，买卖挂单
    bal∈[1B, 9B]： 仓位良好，买卖挂单
    bal∈(0B, 1B)： 仓位较低，买侧挂单，卖侧挂单量减少为bal
    bal∈(9B, 10B)：仓位较高，卖侧挂单，买侧挂单量减少为10-bal
    bal=0B：       买侧挂单，卖侧停止挂单
    bal=10B：      卖侧挂单，买侧停止挂单
将底仓设置为最大仓位，刚好充足。用户亦可以设置更大底仓，如：
最大仓位5B，底仓6B，多余1B对策略无影响，可用于看多梭哈。bal范围将从[0B, 10B]变为[1B, 11B]

若fair_price变动，执行追价操作。即：买单、卖单全部撤单，围绕新的fp重新挂单。
但考虑到fp时常小幅震动，可设定阈值(var)，忽略小幅变动，仅在大幅变动时撤单重挂。
假设初始fp=10000，spread=5，var=2
    fp=10000：buy@9995, sell@10005
    fp=10001：无操作
    fp=10002：无操作
    fp=10003：cancel buy@9995，cancel sell@10005，buy@9998，sell@10008
fp下跌过程类似


-----------运行策略-----------
将api_key写入configs/keys.json
将参数写入configs/spread_maker_xxx.json

命令行执行：python main.py --strategy SpreadMaker --config spread_maker_xxx --key my_key
即可运行策略
main.py可根据需要运行不同的策略、使用不同的参数文件、api_key

新建策略需要注册到strategies.__init__.py中，注册后main.py即可使用
新建参数文件只需放置于configs文件夹中即可
